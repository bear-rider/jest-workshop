# notes

mostly following [this](https://www.alanfoster.me/workshops/jest/workshop-goals/) workshop

## commands

`npm i jest babel-jest babel-core babel-preset-env --save-dev`

`npm run test`

## notes

Babel : transpiler

- plugins: read code and output transformed code
- preset: collection of plugins

      - babel-preset-env : target node or browser or other env
      - babel-preset-react : target react, transfor jsx

Matchers
.toBe(value) : for primitives or strict ===
.toEqual(value) : comparing objects

Async testing

- resolves reject

```javascript
return expect(meaningOfLife()).resolves.toBe(42);
// Note : the return is important!
// Jest needs to know about the promise.
```

- To use ES6 Async/Await you'll need babel
