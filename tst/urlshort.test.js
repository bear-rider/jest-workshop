const shortener = require('../src/urlShort');

test('returns a url', () => {
    expect(shortener("https://www.google.com/")).toMatch(/[0-9]*/);
});