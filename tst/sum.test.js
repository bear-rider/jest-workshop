const sum = require('../src/sum');

test('2 + 2 = 4', () => {
  expect(sum(2, 2)).toBe(4);
});

test('object assignment', () => {
  expect(Number(sum(2, 2))).toEqual(4);
});

test('throws an error', () => {
  expect(() => { throw new Error(); }).toThrow();
  expect(() => { throw new Error(); }).toThrow(Error);

  // You can also use the exact error message or a regexp
  expect(() => { throw new Error("lorem ipsum"); }).toThrow('lorem ipsum');
  expect(() => { throw new Error("lorem ipsum"); }).toThrow(/ipsum/);
});
