"use strict";

const greeter = require("../src/greeter");
const DatabaseMock = require("../src/utils/dbmanager");

describe("greet and persist", () => {

    beforeEach(() => {
        DatabaseMock.saveItem = jest.fn();
    });

    it("greets by name if name given", () => {
        return greeter.greetAndPersist("james").then(greeting => {
            expect(DatabaseMock.saveItem).toBeCalledTimes(1);
            expect(greeting).toBe("hello, james");
        });
    })

    it("greets the world if no name is given", () => {
        return greeter.greetAndPersist('').then(greeting => {
            expect(DatabaseMock.saveItem).toBeCalledTimes(0);
            expect(greeting).toBe("hello, world");
        });
    })

});



describe("been greeted before?", () => {

    it("already greeted", async () => {
        const item = {
            name: "james",
            timestamp: Date.now()
        }
        DatabaseMock.getItem = jest.fn().mockReturnValue(item);

        const result = await greeter.greeted("james");
        expect(result).toBe(true);

    })

});