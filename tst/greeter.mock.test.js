"use strict";

const greeter = require("../src/greeter");
jest.mock("../src/utils/dbmanager");

describe("greet and persist", () => {

    it("greets by name if name given", () => {
        return greeter.greetAndPersist("james").then(greeting => {
            expect(greeting).toBe("hello, james");
        });
    })

    it("greets the world if no name is given", () => {
        return greeter.greetAndPersist('').then(greeting => {
            expect(greeting).toBe("hello, world");
        });
    })

});


describe("been greeted before?", () => {

    it("already greeted", () => {
        return greeter.greeted("james").then(greetedness => {
            expect(greetedness).toBe(false);
        });
    })

});