import meaningOfLife from "../src/meaning";

describe("meaning-of-life-reject-resolve", function () {
    it("calculates the meaning of life", function () {
        return expect(meaningOfLife()).resolves.toBe(42);
        // Note : the return is important!
        // Jest needs to know about the promise.
    });
});

describe("meaning-of-life-async-await", function () {
    it("calculates the meaning of life", async function () {
        const answer = await meaningOfLife();
        expect(answer).toBe(42);
    });
});