"use strict";

const greeter = require("../src/greeter");

describe("say hello", () => {

    it("greets by name if name given", () => {
        const greeting = greeter.greet("zaphod");
        expect(greeting).toBe("hello, zaphod");
    })

    it("greets the world if no name is given", () => {
        const greeting = greeter.greet();
        expect(greeting).toBe("hello, world");
    })

});