const raisedTo = (a, b) => {
  let result = a ** b;
  if (!result) {
    throw new Error();
  } else {
    return result;
  }
}

module.exports = raisedTo
