const sevenAndAHalfMillionYearsLater = 2.3652e14;

export default function () {
    return new Promise(function (resolve, reject) {
        setTimeout(() => resolve(42), sevenAndAHalfMillionYearsLater);
    });
}