'use strict'

const { base62_encode } = require('@samwen/base62-util');


function urlShort(url) {
    const big_slug = base62_encode(url);
    const short_slug = big_slug.slice(0,7);
    return(short_slug);
}

module.exports = urlShort
