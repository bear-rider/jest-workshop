"use strict";

const isValid = (word) => {
    if (word === undefined || word === null || word === '') {
        return false;
    }
    return true;
}

module.exports = { isValid }