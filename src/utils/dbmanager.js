"use strict";

const { DynamoDBClient } = require('@aws-sdk/client-dynamodb-v2-node/DynamoDBClient');

const saveItem = async (item) => {
    const params = {
        TableName: process.env.TABLE,
        Item: item
    }

    const dynamoDB = new DynamoDBClient();
    return await dynamoDB.put(params).promise();
}


const getItem = async (name) => {
    const params = {
        TableName: process.env.TABLE,
        Key: { name: name }
    }

    const dynamoDB = new DynamoDBClient();
    return await dynamoDB.get(params).promise().then(result => result.item);
}


module.exports = {
    saveItem,
    getItem,
}