"use strict";

const db = require("../src/utils/dbmanager");
const { isValid } = require("../src/utils/util");
/**
 * returns hello, name if name is a valid string, else returns hello, world
 * @funtion greet
 * @param   {String} name  valid string.
 * @return {String} greeting name or world
 */
const greet = (name) => {
    if (!isValid(name)) {
        name = 'world'
    }
    return `hello, ${name}`
}

const greetAndPersist = async (name) => {
    if (isValid(name)) {
        await persistGreeting(name);
    }
    return greet(name);
}

const greeted = async (name) => {
    const item = await db.getItem(name);
    console.info(item, "<<<<<<<<<");
    return (item !== undefined) ? true : false;
}

const persistGreeting = async (name) => {
    const item = {
        name: name,
        timestamp: Date.now
    };
    return await db.saveItem(item);

}

module.exports = {
    greet,
    greeted,
    greetAndPersist,
}